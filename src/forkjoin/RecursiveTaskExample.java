package forkjoin;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class RecursiveTaskExample {

    static long numOfOperations = 10_000_000_000L;

    static int numOfThreads = Runtime.getRuntime().availableProcessors();

    static class Task extends RecursiveTask<Long>{

        long from;
        long to;

        Task(long from, long to) {
            this.from = from;
            this.to = to;
        }

        @Override
        protected Long compute() {
            if ((to - from) <= numOfOperations/numOfThreads) {
                long sum = 0;
                for (long i = from; i < to; i++) {
                    sum += i;
                }
                return sum;
            }
            else {
                long middle = (to + from)/2;
                Task firstHalf = new Task(from, middle);
                firstHalf.fork();
                Task secondHalf = new Task(middle + 1, to);
                return firstHalf.join() + secondHalf.compute();
            }
        }
    }

    public static void main(String[] args) {
//        System.out.println(numOfThreads);
        ForkJoinPool forkJoinPool = new ForkJoinPool(numOfThreads);
        System.out.println(forkJoinPool.invoke(new Task(0, numOfOperations)));
    }
}



