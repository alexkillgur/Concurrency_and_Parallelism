package forkjoin.recursiveaction;

import java.util.concurrent.ForkJoinPool;

public class RecursiveActionExample {

    public static void main(String[] args) {
        int[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        ForkJoinPool pool = new ForkJoinPool();
        Square task = new Square(data, 0, data.length);
        pool.invoke(task);

//        ForkJoinPool pool = ForkJoinPool.commonPool();
//        Square task = new Square(data, 0, data.length);
//        pool.invoke(task);

//         Square task = new Square(data, 0, data.length);
//        task.invoke();

        System.out.println(task.result);
    }
}




