package forkjoin.recursiveaction;

import java.util.concurrent.RecursiveAction;

class Square extends RecursiveAction {

    final int LIMIT = 3;
    static int result;
    int start, end;
    int[] data;

    Square(int[] data, int start, int end) {
        this.start = start;
        this.end = end;
        this.data = data;
    }

    @Override
    protected void compute() {
        if ((end - start) < LIMIT) {
            for (int i = start; i < end; i++) {
                result += data[i] * data[i];
            }
        } else {
            int mid = (start + end) / 2;

            Square left = new Square(data, start, mid);
            Square right = new Square(data, mid, end);
            left.fork();
            right.fork();
            left.join();
            right.join();

//            invokeAll( new Square(data, start, mid), new Square(data, mid, end));
        }
    }
}
