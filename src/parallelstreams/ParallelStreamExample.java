package parallelstreams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class ParallelStreamExample {
    //static volatile List<Integer> ints = new ArrayList<>();

    public static void main(String[] args) {
        //List<Integer> ints = new ArrayList<>();
        List<Integer> ints = Collections.synchronizedList(new ArrayList<>());

        IntStream.range(0, 1000000)
                .parallel()
                .forEach(ints::add);
        System.out.println(ints.size());

        //Aggregate operations and parallel streams enable you to implement parallelism with non-thread-safe collections
        //provided that you do not modify the collection while you are operating on it.

        double start = System.nanoTime();
        long c = IntStream.range(0, 2000000)
                //.parallel()
                .limit(1000000)
                //.parallel()
                .sum();
        double duration = (System.nanoTime() - start) / 1_000_000;
        System.out.println("Got " + c + " in " + duration + " milliseconds");

        List<Integer> ints1 = new ArrayList<>();
        IntStream.range(0, 10).forEach(ints1::add);

        ints1.parallelStream().forEach(integer -> System.out.print(integer + "   "));
        System.out.println();
        ints1.stream().parallel().forEach(integer -> System.out.print(integer + "   "));
    }
}
