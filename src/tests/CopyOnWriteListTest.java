package tests;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CopyOnWriteListTest {
    private static List<String> listNotSync;
    private static List<String> listSync;
    private static List<Integer> numbers;

    private static void iterate(Iterator<String> iterator, List<String> list) {
        while (iterator.hasNext()) {
            String str = iterator.next();
            if (str.equals("2")) list.remove("5");
            if (str.equals("3")) list.add("3 found");
            if (str.equals("4")) list.set(1, "4");
        }
    }

    @BeforeAll
    public static void setup() {
        // http://www.baeldung.com/java-copy-on-write-arraylist
        listNotSync = new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5"));
        listSync = new CopyOnWriteArrayList<>(Arrays.asList("1", "2", "3", "4", "5"));
        numbers = new CopyOnWriteArrayList<>(Arrays.asList(1, 3, 5, 8));
    }

    @Test
    public void addTest() {
        Iterator<Integer> iterator = numbers.iterator();

        numbers.add(10);

        List<Integer> result = new LinkedList<>();
        iterator.forEachRemaining(result::add);

        assertThat(result).containsOnly(1, 3, 5, 8);

        Iterator<Integer> iterator2 = numbers.iterator();

        List<Integer> result2 = new LinkedList<>();
        iterator2.forEachRemaining(result2::add);

        assertThat(result2).containsOnly(1, 3, 5, 8, 10);
    }

    @Test
    public void removeTest() {
        Iterator<Integer> iterator = numbers.iterator();

        Executable removeTest = () -> {
            while (iterator.hasNext()) {
                iterator.remove();
            }
        };

        assertThrows(UnsupportedOperationException.class, removeTest);
    }

    @Test
    public void modificationNotSyncTest() {
        Iterator<String> it = listNotSync.iterator();

        Executable modificationTest = () -> iterate(it, listNotSync);

        assertThrows(ConcurrentModificationException.class, modificationTest);
    }

    @Test
    public void modificationSyncTest() {
        Iterator<String> it = listSync.iterator();

        iterate(it, listSync);

        assertThat(listSync).containsOnly("1", "4", "3", "4", "3 found");
    }
}