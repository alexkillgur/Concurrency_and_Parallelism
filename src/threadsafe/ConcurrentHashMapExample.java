package threadsafe;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class ConcurrentHashMapDemo {  //1.5
    private static final Map<Integer,String> map = new ConcurrentHashMap<>();
    public static void main(String[] args) {
        TestClass.start(map);
    }

}

class ConcurrentHashTable {
    private static final Hashtable<Integer,String> map = new Hashtable<>();

    public static void main(String[] args) {
        TestClass.start(map);
    }
}





class TestClass{


    public static void start(Map map){
        ExecutorService service = Executors.newFixedThreadPool(3);

        service.execute(() -> {
            for(int i= 1; i<=1000; i++) {
                map.putIfAbsent(i, "A"+ i);
            }
        });

        service.execute(() -> {
            for(int i= 1; i<=500; i++) {
                map.put(i, "B"+ i);
            }
        });
        service.execute(() -> {
            Iterator<Integer> ite = map.keySet().iterator();
            while(ite.hasNext()){
                Integer key = ite.next();
                System.out.println(key+" : " + map.get(key));
            }
        });
        service.shutdownNow();
    }
}



//HashTable  1.1
//HashMap 1.2
//Collections.synchronizedMap  // since 1.2  The only difference between Hashtable and Synchronized Map is that later is not a legacy and you can wrap any Map to create it’s synchronized version by using Collections.synchronizedMap() method.



//        ConcurrentHashMap implements Map data structure and also provide thread safety like Hashtable.
//        It works by dividing complete hashtable array in to segments or portions and allowing parallel access to those segments.
//        The locking is at a much finer granularity at a hashmap bucket level.
//        Use ConcurrentHashMap when you need very high concurrency in your application.
//        It is thread-safe without synchronizing the whole map.
//        Reads can happen very fast while write is done with a lock on segment level or bucket level.
//        There is no locking at the object level.
//        ConcurrentHashMap doesn’t throw a ConcurrentModificationException if one thread tries to modify it while another is iterating over it.
//        ConcurrentHashMap does not allow NULL values, so key can not be null in ConcurrentHashMap
//        ConcurrentHashMap doesn’t throw a ConcurrentModificationException if one thread tries to modify it, while another is iterating over it.
